<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Sanctum\HasApiTokens;

/**
 * @OA\Schema(
 *     @OA\Property(
 *         property="id",
 *         description="ID of {User}",
 *         type="integer",
 *         format="int64",
 *         default="1",
 *     ),
 *
 *     @OA\Property(
 *         property="first_name",
 *         description="First name of {User}",
 *         type="string",
 *         format="string",
 *         default="John",
 *     ),
 *
 *     @OA\Property(
 *         property="last_name",
 *         description="Last name of {User}",
 *         type="string",
 *         format="string",
 *         default="Doe",
 *     ),
 *
 *     @OA\Property(
 *         property="email",
 *         description="E-mail of {User}",
 *         type="string",
 *         format="email",
 *         default="john.doe@example.org",
 *     ),
 *
 *     @OA\Property(
 *         property="password",
 *         description="Password of {User}",
 *         type="string",
 *         format="password",
 *         default="pass123456",
 *     ),
 *
 *     required={
 *         "first_name",
 *         "last_name",
 *         "email",
 *         "password",
 *     }
 * )
 */
class User extends Authenticatable
{
    use HasApiTokens, HasFactory, SoftDeletes;

    protected $guarded = [];

    protected $hidden = [
        'password',
    ];
}
