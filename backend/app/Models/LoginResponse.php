<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @OA\Schema(
 *     @OA\Property(
 *         property="user",
 *         description="Authenticated user",
*          ref="#/components/schemas/User",
 *     ),
 *
 *     @OA\Property(
 *         property="token",
 *         description="Token of authenticated user",
 *         type="string",
 *         format="string",
 *     ),
 *
 *     required={
 *         "user",
 *         "token",
 *     }
 * )
 */
class LoginResponse extends Model
{
}
