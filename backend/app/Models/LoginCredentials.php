<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @OA\Schema(
 *     @OA\Property(
 *         property="email",
 *         description="Email of registrated user",
 *         type="string",
 *         format="email",
 *         default="john.doe@example.org",
 *     ),
 *
 *     @OA\Property(
 *         property="password",
 *         description="Password of registrated user",
 *         type="string",
 *         format="password",
 *         default="test123456",
 *     ),
 *
 *     required={
 *         "email",
 *         "password",
 *     }
 * )
 */
class LoginCredentials extends Model
{
}
