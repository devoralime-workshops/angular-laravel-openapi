<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Support\Facades\Auth;

class SampleDataController extends Controller
{
    /**
     * @OA\Get(
     *     path="/sample-data",
     *     tags={"Sample Data"},
     *     summary="Get sample data for authenticated {User}",
     *     description="Get sample data for authenticated {User}",
     *     operationId="getSampleData",
     *
     *     @OA\Response(
     *         response=200,
     *         description="Sample data retrieved",
     *     ),
     * )
     *
     * Get sample data for authenticated {User}.
     *
     * @return \Illuminate\Http\Response
     */
    public function getSampleData()
    {
        return response()->json('Sample data');
    }
}
