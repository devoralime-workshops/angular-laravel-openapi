<?php

namespace App\Http\Controllers;

use App\Http\Requests\LoginUserRequest;
use App\Http\Requests\RegisterUserRequest;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;

class AuthController extends Controller
{
    /**
     * @OA\Post(
     *     path="/auth/user/login",
     *     tags={"User"},
     *     summary="Login for {User}",
     *     description="Login for {User}",
     *     operationId="login",
     *
     *     @OA\RequestBody(
     *         required=true,
     *
     *         @OA\JsonContent(
     *             ref="#/components/schemas/LoginCredentials",
     *         ),
     *     ),
     *
     *     @OA\Response(
     *         response=200,
     *         description="User logged in",
     *
     *         @OA\JsonContent(
     *             ref="#/components/schemas/LoginResponse",
     *         ),
     *     ),
     *
     *     @OA\Response(
     *         response=401,
     *         description="Invalid login credentials",
     *     ),
     * )
     *
     * Login for {User}.
     *
     * @param  \App\Requests\LoginUserRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function login(LoginUserRequest $request)
    {
        if (Auth::attempt($request->all())) {
            $user = User::find(Auth::id());
            $token = $user->createToken('auth_token', ['authentication'])->plainTextToken;

            return response()->json([
                'user' => $user,
                'token' => $token,
            ]);
        } else {
            return response()->json([ 'message' => 'Invalid login credentials' ], 401);
        }
    }


    /**
     * @OA\Post(
     *     path="/auth/user/register",
     *     tags={"User"},
     *     summary="Register a new {User}",
     *     description="Register a new {User}",
     *     operationId="register",
     *
     *     @OA\RequestBody(
     *         required=true,
     *
     *         @OA\JsonContent(
     *             ref="#/components/schemas/User",
     *         ),
     *     ),
     *
     *     @OA\Response(
     *         response=200,
     *         description="New user created",
     *     ),
     * )
     *
     * Register a new {User}.
     *
     * @param  \App\Requests\RegisterUserRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function register(RegisterUserRequest $request)
    {
        User::create([
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
        ]);

        return response()->json('New user created');
    }
}
