<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\SampleDataController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(["prefix" => "auth"], function () {
    Route::group(["prefix" => "user"], function () {
        Route::post("login", [AuthController::class, "login"]);
        Route::post("register", [AuthController::class, "register"]);
    });
});

Route::middleware(['auth:sanctum', 'ability:authentication'])->group(function () {
    Route::get("sample-data", [SampleDataController::class, "getSampleData"]);
});
