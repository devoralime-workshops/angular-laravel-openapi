/**
 * OpenAPI Test API
 * API for OpenAPI Test
 *
 * The version of the OpenAPI document: 1.0.0
 * Contact: kiss.bence@devoralime.com
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */


export interface LoginCredentials { 
    /**
     * Email of registrated user
     */
    email: string;
    /**
     * Password of registrated user
     */
    password: string;
}

