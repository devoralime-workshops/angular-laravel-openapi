export * from './sampleData.service';
import { SampleDataService } from './sampleData.service';
export * from './sampleData.serviceInterface';
export * from './user.service';
import { UserService } from './user.service';
export * from './user.serviceInterface';
export const APIS = [SampleDataService, UserService];
