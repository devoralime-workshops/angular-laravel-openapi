import { Component, OnInit } from '@angular/core';
import { SampleDataService } from 'api';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {
  constructor(private _sampleDataService: SampleDataService) {}

  public sampleData?: string;

  ngOnInit(): void {
    this._sampleDataService.getSampleData().subscribe({
      next: (response) => {
        this.sampleData = response;
      },
    });
  }
}
