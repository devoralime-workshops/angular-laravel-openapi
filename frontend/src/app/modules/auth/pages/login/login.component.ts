import { Component } from '@angular/core';
import { Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ControlsOf, FormControl, FormGroup } from '@ngneat/reactive-forms';
import { LoginCredentials, UserService } from 'api';

@Component({
  selector: 'app-login-page',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginPageComponent {
  constructor(private _router: Router, private _userService: UserService) {}

  public loginForm = new FormGroup<ControlsOf<LoginCredentials>>({
    email: new FormControl('', [Validators.required, Validators.email]),
    password: new FormControl('', Validators.required),
  });

  public login() {
    if (this.loginForm.invalid) {
      return;
    }

    const loginCredentials: LoginCredentials = this.loginForm.value;

    this._userService.login({ loginCredentials }).subscribe({
      next: (response) => {
        localStorage.setItem('user', JSON.stringify(response.user));
        localStorage.setItem('token', response.token);
        this._router.navigate(['/']);
      },
    });
  }
}
