import { Component } from '@angular/core';
import { Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { ControlsOf, FormControl, FormGroup } from '@ngneat/reactive-forms';
import { User, UserService } from 'api';

@Component({
  selector: 'app-registration-page',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.scss'],
})
export class RegistrationPageComponent {
  constructor(
    private _router: Router,
    private _route: ActivatedRoute,
    private _snackBar: MatSnackBar,
    private _userService: UserService
  ) {}

  public registrationForm = new FormGroup<ControlsOf<User>>({
    first_name: new FormControl('', Validators.required),
    last_name: new FormControl('', Validators.required),
    email: new FormControl('', [Validators.required, Validators.email]),
    password: new FormControl('', Validators.required),
  });

  public register() {
    if (this.registrationForm.invalid) {
      return;
    }

    const user: User = this.registrationForm.value;

    this._userService.register({ user }).subscribe({
      next: (response) => {
        this._router
          .navigate(['login'], { relativeTo: this._route })
          .then(() => {
            this._snackBar.open(response, 'OK', {
              duration: 5000,
              horizontalPosition: 'right',
              verticalPosition: 'top',
            });
          });
      },
    });
  }
}
