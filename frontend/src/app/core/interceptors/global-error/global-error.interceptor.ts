import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpErrorResponse,
} from '@angular/common/http';
import { catchError, Observable, of } from 'rxjs';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';

@Injectable()
export class GlobalErrorInterceptor implements HttpInterceptor {
  constructor(private _router: Router, private _snackBar: MatSnackBar) {}

  intercept(
    request: HttpRequest<unknown>,
    next: HttpHandler
  ): Observable<HttpEvent<unknown>> {
    return next.handle(request).pipe(
      catchError((errorResponse: HttpErrorResponse) => {
        this._snackBar.open(errorResponse.error.message, 'OK', {
          duration: 5000,
          horizontalPosition: 'right',
          verticalPosition: 'top',
        });

        if (errorResponse.status === 401) {
          localStorage.clear();
          this._router.navigate(['/auth/login']);
        }

        return of();
      })
    );
  }
}
